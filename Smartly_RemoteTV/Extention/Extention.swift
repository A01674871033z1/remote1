//
//  Extention.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 13/2/2022.
//


import Foundation
import Foundation
import MBProgressHUD
import SwiftMessages

extension String {
    func toUIImage() -> UIImage? {
        return UIImage(named: self)
    }
    func toUIFont(size: CGFloat) -> UIFont? {
        return UIFont(name: self, size: size)
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
}

extension String {
    func getFavouriteList() -> [String] {
        let favourites = AppManager.shared().favouriteApps
        if favourites.isEmpty {
            return []
        }
        return favourites.components(separatedBy: "=")
    }
    func isFavourited() -> Bool {
        getFavouriteList().contains(self)
    }
    func removeFavourite() {
        let list = getFavouriteList().filter{ $0 != self }
        AppManager.shared().favouriteApps = list.joined(separator: "=")
    }
    func addFavourite() {
        var list = getFavouriteList()
        list.append(self)
        AppManager.shared().favouriteApps = list.joined(separator: "=")
    }
}


extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}

/// Scope functions
protocol ScopeFunc { }

extension ScopeFunc {
    /// Calls the specified function block with self value as its argument and returns self value.
    @inline(__always) func apply(block: (Self) -> ()) -> Self {
        block(self)
        return self
    }
    
    /// Calls the specified function block with self value as its argument and returns its result.
    @discardableResult
    @inline(__always) func letIt<R>(block: (Self) -> R) -> R {
        return block(self)
    }
}

extension NSObject: ScopeFunc { }

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
        return ceil(boundingBox.width)
    }
}

extension NSObject {
    
    var className: String {
        
        return String(describing: type(of: self))
    }
    
    class var className: String {
        
        return String(describing: self)
    }
    
}

extension String {
    
    func nibWithNameiPad()->String{
        var str = self
        if IS_IPAD{
            str = str + "_iPad"
        }
        return str
    }
}

extension UIViewController {
    func isModal() -> Bool {
        if self.presentingViewController != nil {
            return true
        } else if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        } else if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
    
    func showLoading(){
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    func hideLoading(){
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    func showError(message: String){
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureTheme(.error)
        success.configureDropShadow()
        success.configureContent(title: "Error", body: message)
        success.button?.isHidden = true
        
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    func showSuccess(message: String){
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureTheme(.success)
        success.configureDropShadow()
        success.configureContent(title: "Success", body: message)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    func currentView() -> UIViewController{
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }

            return topController
        }
        return (keyWindow?.rootViewController)!
    }
}

extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        
        guard let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?.first as? T else {
            return T()
        }
        
        return view
    }
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func layerGradient(colors: [CGColor]) {
        let layer : CAGradientLayer = CAGradientLayer()
        layer.frame.size = self.frame.size
        layer.frame.origin = CGPoint.init(x: 0, y: 0)
        layer.colors = colors
        self.layer.insertSublayer(layer, at: 0)
    }
    
    class func loadFromNibNamed(nibNamed: String, bundle: Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
        
    func addTapGesture(callBack: Selector) -> Void {
        let tap = UITapGestureRecognizer.init(target: self, action: callBack)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    func addTapGesture(callBack: Selector,parent:AnyObject?) -> Void {
        let tap = UITapGestureRecognizer.init(target: parent, action: callBack)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    func addTapGesture(callBack: Selector,parent:AnyObject?,delegate:UIGestureRecognizerDelegate) -> Void {
        let tap = UITapGestureRecognizer.init(target: parent, action: callBack)
        tap.delegate = delegate
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    func addTapGesture(callBack: Selector,viewController:UIViewController) -> Void {
        let tap = UITapGestureRecognizer.init(target: viewController, action: callBack)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    
    @IBInspectable var cornerRadiusV: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidthV: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColorV: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UIFont {
   
    class func applicationFont(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: size)!
    }
    
    class func boldApplicationFont(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-SemiBold", size: size)!
    }
    
    class func mediumApplicationFont(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Medium", size: size)!
    }

    class func lightApplicationFont(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Light", size: size)!
    }
}
extension UIView {
    var parentVC3: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
//extension HomeViewController: UITextFieldDelegate{
//    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true;
//    }
//}
