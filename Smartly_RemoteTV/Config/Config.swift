//
//  Config.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import Foundation

import UIKit
import UIColor_Hex_Swift

var DEVICE_WIDTH = UIScreen.main.bounds.width
var DEVICE_HEIGHT = UIScreen.main.bounds.height
let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad

let appid = "1590713779"
let mailSupport = "nguyenvietdat010620@gmail.com"

//Key Admob iOS 2:
let admobBanner = "ca-app-pub-7204820750600293/4059763738"
let admobFull = "ca-app-pub-7204820750600293/1242028704"
let adNativeAd = ""
let openAds = "ca-app-pub-7204820750600293/1891931241"
var numberToShowAd = 5
let AppBackgroundColor = UIColor("#F0F0F3")
let AppColor = UIColor("#3566E1")

//MARK: -- Font
let AppFont = UIFont.applicationFont(17)

