//
//  Constants.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import Foundation
import UIKit

enum Constants {
    static let appId = ""
    static let sourceId = "com.webos.app.inputmgr"
    static let guideId = "com.webos.app.tvuserguide"
    
    enum Apps: String, CaseIterable {
        case amazone = "amazon"
        case appleTV = "com.apple.appletv"
        case bluetoothSound = "com.webos.app.btspeakerapp"
        case brandShop = "com.webos.app.brandshop"
        case broadcastProgram = "com.webos.app.livetv"
        case calendar = "com.webos.app.scheduler"
        case chanelManager = "com.webos.app.channeledit"
        case cheeringMode = "com.webos.app.cheeringtv"
        case customerSupport = "com.webos.app.customersupport"
        case deviceConnector = "com.webos.app.connectionwizard"
        case facebook = "com.webos.app.iot-thirdparty-login"
        case findChanel = "com.webos.app.channelsetting"
        case fptPlay = "com.fpt.fptplay"
        case iQIYI = "com.iq.app.iqiyiapp"
        case lgContentStorename = "com.webos.app.discovery"
        case liveplus = "com.webos.app.acrcard"
        case lgThinQ = "com.webos.app.actionhandler"
        case media = "com.webos.app.photovideo"
        case mp3 = "com.webos.app.music"
        case myTv = "com.mytvb2c.app"
        case netflix = "netflix"
        case remoteManagement = "com.webos.app.care365"
        case searchApp = "com.webos.app.voice"
        case settingApp = "com.palm.app.settings"
        case tmall = "com.webos.app.alibabafull"
        case viewOn = "vieplay.vn"
        case websiteBrowser = "com.webos.app.browser"
        case websiteApp = "com.webos.app.webapphost"
        case youtube = "youtube.leanback.v4"
        case av = "com.webos.app.externalinput.av1"
        case googleAssistant = "com.webos.app.googleassistant"
        case hdmi1 = "com.webos.app.hdmi1"
        case home = "com.webos.app.home"
        case houseControl = "com.webos.app.homeconnect"
        case LiveCUDB = "com.webos.app.livecudb"
        case LiveHbbTV = "com.webos.app.livehbbtv"
        case infoTv = "com.webos.app.livemenu"
        case magicNumber = "com.webos.app.magicnum"
        case lgMemberShip = "com.webos.app.membership"
        case screenShare = "com.webos.app.miracast"
        case oneTouchSoundTuning = "com.webos.app.onetouchsoundtuning"
        case remoteService = "com.webos.app.remoteservice"
        case remoteSetting = "com.webos.app.remotesetting"
        case quickHelp = "com.webos.app.self-diagnosis"
        case softWareUpdate = "com.webos.app.softwareupdate"
        case musicOverlay = "com.webos.app.systemmusic"
        case magicLink = "com.webos.app.tips"
        case localControlPanel = "com.webos.app.tvhotkey"
        case tutorial = "com.webos.app.tvuserguide"
        case twitch = "tv.twitch.tv.starshot.lg"
        case galaxyPlay = "vn.fimplus.movies"
        case theFirstUse = "com.palm.app.firstuse"
    }
    
    enum RemoteControl {
        case power
        case left
        case right
        case down
        case up
        case ok
        case back
        case exit
        case source(String)
        case guide(String)
        case mute
        case voice
        case previous
        case next
        case volumeUp
        case volumeDown
        case chUp
        case chDown
        case home
        case pause
        case play
        case number(UInt)
        case numberDelete
        case numberNone
        case click
        case move(CGFloat, CGFloat)
        case scroll(CGFloat, CGFloat)
    }
}
