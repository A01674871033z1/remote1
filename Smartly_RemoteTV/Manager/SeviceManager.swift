//
//  SeviceManager.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import Foundation
import ConnectSDK

typealias ErrorVoid = (Error?) -> Void

enum LGState {
    case onDeviceReady
    case onDeviceDisconnect
    case onParingRequire(DeviceServicePairingType)
    case onCapabilityUpdated
    case onConnectionFailed(String)
    case connectableDevicePairingSuccess
    case connectableDeviceConnectionSuccess
    case connectableDeviceConnectionRequired
}

class ServiceManager: NSObject {
    
    static let shared = ServiceManager()
    
    private var delegate: ((LGState) -> Void)?
    var connectableDevice: ConnectableDevice?
    var service: WebOSTVService? = nil
    
    func disconnect() {
        print("disconnect")
        service?.disconnectMouse()
        service?.disconnect()
        connectableDevice?.disconnect()
        removeCurrentService()
    }
    
    func connect() {
        if connectableDevice == nil{
            return
        }
        print("connect")
        let service = connectableDevice?.services
            .compactMap{ $0 as? WebOSTVService}
            .first
        self.service = service
        addService()
        connectableDevice?.setPairingType(DeviceServicePairingTypePinCode)
        connectableDevice?.connect()
    }
    
    private func addService() {
        if connectableDevice != nil,
           service != nil {
            connectableDevice?.addService(service)
            connectableDevice?.delegate = self
            service?.delegate = self
            service?.connect()
            service?.connectMouse(success: nil, failure: nil)
        }
    }
    
    private func removeCurrentService() {
        if ((connectableDevice?.hasServices) == true) {
            let serviceId = connectableDevice?.connectedServiceNames()
            connectableDevice?.removeService(withId: serviceId)
        }
    }
    
    func subscribeMute(onSuccess: @escaping (Bool) -> Void, onFailure: @escaping ErrorVoid = {_ in} ) {
        service?.subscribeMute(success: { result in
            onSuccess(result)
        }, failure: { error in
            onFailure(error)
        })
    }
    
    func getAppList(onSuccess: @escaping ([AppInfo]) -> Void, onFailure: @escaping ErrorVoid = {_ in}) {
        service?.getAppList(success: {
            onSuccess(
                $0?.compactMap { $0 as? AppInfo }
                    .filter { !$0.name.isEmpty } ?? []
            )
        }, failure: { error in
            onFailure(error)
        })
    }
    
    func launchApp(id: String, onSuccess: @escaping (LaunchSession?) -> Void = {_ in}, onFailure: @escaping ErrorVoid = {_ in }) {
        service?.launchApp(id, success: { onSuccess($0) }, failure: { onFailure($0) })
    }
    
    func delegate<T: AnyObject>(_ object: T, equalTo: @escaping (T, LGState) -> Void) {
        self.delegate = { [weak object] in
            if let object = object {
                equalTo(object, $0)
            }
        }
    }
    
}

//MARK: -- DeviceServiceDelegate

extension ServiceManager: DeviceServiceDelegate {
    func deviceServicePairingSuccess(_ service: DeviceService!) {
        print("deviceServicePairingSuccess")
        delegate?(LGState.connectableDevicePairingSuccess)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectedDevice"), object: nil)
    }
    func deviceServiceConnectionSuccess(_ service: DeviceService!) {
        print("deviceServiceConnectionSuccess")
    }
}

//MARK: -- ConnectableDeviceDelegate

extension ServiceManager: ConnectableDeviceDelegate {
    func connectableDeviceReady(_ device: ConnectableDevice!) {
        print("connectableDeviceReady")
        delegate?(LGState.onDeviceReady)
    }
    func connectableDevicePairingSuccess(_ device: ConnectableDevice!, service: DeviceService!) {
        print("connectableDevicePairingSuccess")
        AppManager.shared().recentDeviceId = device.id
        delegate?(LGState.connectableDevicePairingSuccess)
    }
    func connectableDeviceDisconnected(_ device: ConnectableDevice!, withError error: Error!) {
        print("connectableDeviceDisconnected")
        //        if let currentDevice = self.connectableDevice, currentDevice.id == device.id{
        //            connectableDevice = nil
        //            disconnect()
        //        }
        delegate?(LGState.onDeviceDisconnect)
    }
    func connectableDevice(_ device: ConnectableDevice!, connectionFailedWithError error: Error!) {
        print("connectionFailedWithError")
        delegate?(LGState.onConnectionFailed(error.localizedDescription))
    }
    func connectableDeviceConnectionSuccess(_ device: ConnectableDevice!, for service: DeviceService!) {
        print("connectableDeviceConnectionSuccess")
        delegate?(LGState.connectableDeviceConnectionSuccess)
    }
    
    func connectableDeviceConnectionRequired(_ device: ConnectableDevice!, for service: DeviceService!) {
        print("connectableDeviceConnectionRequired")
        delegate?(LGState.connectableDeviceConnectionRequired)
    }
    func connectableDevice(_ device: ConnectableDevice!, capabilitiesAdded added: [Any]!, removed: [Any]!) {
        print("capabilitiesAdded")
        delegate?(LGState.onCapabilityUpdated)
    }
    func connectableDevice(_ device: ConnectableDevice!, service: DeviceService!, pairingFailedWithError error: Error!) {
        print("pairingFailedWithError")
        if error.localizedDescription.contains("User denied access") {
            delegate?(LGState.onConnectionFailed("User denied access"))
        }
    }
    func connectableDevice(_ device: ConnectableDevice!, service: DeviceService!, didFailConnectWithError error: Error!) {
        print("didFailConnectWithError")
        delegate?(LGState.onConnectionFailed(error.localizedDescription))
    }
    func connectableDevice(_ device: ConnectableDevice!, service: DeviceService!, pairingRequiredOfType pairingType: Int32, withData pairingData: Any!) {
        print("pairingRequiredOfType pairingType")
        delegate?(LGState.onParingRequire(DeviceServicePairingType(rawValue: UInt32(pairingType))))
    }
}

extension ServiceManager {
    
    func handle(_ key: Constants.RemoteControl) {
        switch key {
        case .click:
            service?.click(success: nil, failure: nil)
        case .move(let dx, let dy):
            service?.move(CGVector(dx: dx, dy: dy), success: nil, failure: nil)
        case .scroll(let dx, let dy):
            service?.scroll(CGVector(dx: dx, dy: dy), success: nil, failure: nil)
        case .power:
            service?.powerOff(success: nil, failure: nil)
        case .left:
            service?.left(success: nil, failure: nil)
        case .right:
            service?.right(success: nil, failure: nil)
        case .down:
            service?.down(success: nil, failure: nil)
        case .up:
            service?.up(success: nil, failure: nil)
        case .ok:
            service?.ok(success: nil, failure: nil)
        case .back:
            service?.back(success: nil, failure: nil)
        case .exit:
            service?.home(success: nil, failure: nil)
        case .source(let id), .guide(let id):
            service?.launchApp(id, success: nil, failure: nil)
        case .mute:
            service?.setMute(!AppManager.shared().isMute, success: nil, failure: nil)
        case .voice:
            break
        case .previous:
            service?.rewind(success: nil, failure: nil)
        case .next:
            service?.fastForward(success: nil, failure: nil)
        case .volumeUp:
            service?.volumeUp(success: nil, failure: nil)
        case .volumeDown:
            service?.volumeDown(success: nil, failure: nil)
        case .chUp:
            service?.channelUp(success: nil, failure: nil)
        case .chDown:
            service?.channelDown(success: nil, failure: nil)
        case .home:
            service?.home(success: nil, failure: nil)
        case .pause:
            service?.pause(success: nil, failure: nil)
        case .play:
            service?.play(success: nil, failure: nil)
        case .number(let number):
            var buttonNumber = WebOSTVMouseButton0
            switch number {
            case 1:
                buttonNumber = WebOSTVMouseButton1
            case 2:
                buttonNumber = WebOSTVMouseButton2
            case 3:
                buttonNumber = WebOSTVMouseButton3
            case 4:
                buttonNumber = WebOSTVMouseButton4
            case 5:
                buttonNumber = WebOSTVMouseButton5
            case 6:
                buttonNumber = WebOSTVMouseButton6
            case 7:
                buttonNumber = WebOSTVMouseButton7
            case 8:
                buttonNumber = WebOSTVMouseButton8
            case 9:
                buttonNumber = WebOSTVMouseButton9
                
            default:
                break
            }
            service?.mouseSocket.button(buttonNumber)
        case .numberDelete:
            service?.mouseSocket.button(WebOSTVMouseButtonDelete)
        case .numberNone:
            service?.mouseSocket.button(WebOSTVMouseButtonDash)
            //        case .number(_):
            //            print("hung")
            //        case .numberDelete:
            //            print("hung")
            //        case .numberNone:
            //            print("hung")
        }
    }
}
