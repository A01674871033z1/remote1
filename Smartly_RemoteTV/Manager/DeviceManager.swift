//
//  DeviceManager.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import Foundation
import ConnectSDK

protocol DeviceManagerDelegate: AnyObject {
    func didFindDevice(_ device: ConnectableDevice)
    func didUpdateDevice(_ device: ConnectableDevice)
    func didLostDevice(_ device: ConnectableDevice)
    func didFailWithError(_ error: Error)
}

class DeviceManager: NSObject {
    
    weak var delegate: DeviceManagerDelegate?
    static let shared = DeviceManager()
    var devices:[ConnectableDevice] = []
    
    func startScanDevice(){
        devices.removeAll()
        DiscoveryManager.shared().delegate = self
        DiscoveryManager.shared().registerDefaultServices()
        if DiscoveryManager.shared().pairingLevel != DeviceServicePairingLevelOn {
            DiscoveryManager.shared().pairingLevel = DeviceServicePairingLevelOn
        }
        DiscoveryManager.shared().startDiscovery()
    }
    
    func stopScanDevice(){
        DiscoveryManager.shared().stopDiscovery()
    }
        
    private func addOrUpdateDevices(device: ConnectableDevice) {
        let acceptDevice = !device.services
            .compactMap { $0 as? WebOSTVService }
            .isEmpty
        
        if device.modelNumber != nil, acceptDevice {
            let ids = devices.map({$0.id})
            if let index = ids.firstIndex(of: device.id) {
                devices[index] = device
            }else{
                devices.append(device)
            }
        }
    }
    
    private func removeDevices(device: ConnectableDevice){
        let ids = devices.map({$0.id})
        if let index = ids.firstIndex(of: device.id) {
            devices.remove(at: index)
        }
    }
}

//MARK: -- DiscoveryManagerDelegate

extension DeviceManager : DiscoveryManagerDelegate {
    
    func discoveryManager(_ manager: DiscoveryManager!, didFind device: ConnectableDevice!) {
        print("discoveryManager didFind")
        addOrUpdateDevices(device: device)
        delegate?.didFindDevice(device)
    }
    
    func discoveryManager(_ manager: DiscoveryManager!, didLose device: ConnectableDevice!) {
        print("discoveryManager didLose")
        removeDevices(device: device)
        delegate?.didLostDevice(device)
    }
    
    func discoveryManager(_ manager: DiscoveryManager!, didUpdate device: ConnectableDevice!) {
        print("discoveryManager didUpdate")
        addOrUpdateDevices(device: device)
        delegate?.didUpdateDevice(device)
    }
    
    func discoveryManager(_ manager: DiscoveryManager!, didFailWithError error: Error!) {
        print("discoveryManager didFailWithError")
        delegate?.didFailWithError(error)
    }
}

