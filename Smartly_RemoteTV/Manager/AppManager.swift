//
//  AppManager.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import Foundation
final class AppManager {
    
    enum UserDefualtManagerKey: String {
        case numberOfLaunches
        case isMute
        case isConnected
        case isReConnected
        case isDeviceOn
        case isLaunchApp
        case recentDeviceId
        case favouriteApps
        case lastTimeInBackground
    }
    
    var userDefaults: UserDefaults = .standard
    
    private static var shareInstance: AppManager = {
        let User = AppManager.init()
        return User
    }()
    
    final class func shared() -> AppManager {
        return shareInstance
    }
    
    var isMute: Bool {
        get { return value(for: UserDefualtManagerKey.isMute.rawValue) ?? false }
        set { updateDefaults(for: UserDefualtManagerKey.isMute.rawValue, value: newValue) }
    }
    
    var recentDeviceId: String {
        get { return value(for: UserDefualtManagerKey.recentDeviceId.rawValue) ?? "" }
        set { updateDefaults(for: UserDefualtManagerKey.recentDeviceId.rawValue, value: newValue) }
    }
    
    var favouriteApps: String {
        get { return value(for: UserDefualtManagerKey.favouriteApps.rawValue) ?? "" }
        set { updateDefaults(for: UserDefualtManagerKey.favouriteApps.rawValue, value: newValue) }
    }
}


extension AppManager {
    
    private func updateDefaults(for key: String, value: Any) {
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    
    private func value<T>(for key: String) -> T? {
        return userDefaults.value(forKey: key) as? T
    }
    
}
