//
//  ControlV.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 28/11/2021.
//

import UIKit

class ControlV: UIView {
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var returnButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var inputButton: UIButton!

    override class func awakeFromNib() {
        super.awakeFromNib()
    }

}
