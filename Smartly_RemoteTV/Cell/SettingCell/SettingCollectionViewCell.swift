//
//  SettingCollectionViewCell.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit

class SettingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblNameSetting:UILabel!
    @IBOutlet weak var ImageViewa:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if UIDevice.current.userInterfaceIdiom == .pad{
            lblNameSetting.font = lblNameSetting.font.withSize(22)
        }
    }

}
