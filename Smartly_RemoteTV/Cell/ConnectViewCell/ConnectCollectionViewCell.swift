//
//  ConnectCollectionViewCell.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit

class ConnectCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        colorView.cornerRadiusV = 20
    }
    func updateCell(name: String) {
        if !name.lowercased().contains("lg") {
            self.nameLabel.text = "[LG] webOS TV \(name)"
        } else {
            self.nameLabel.text = name
        }
    }

}
