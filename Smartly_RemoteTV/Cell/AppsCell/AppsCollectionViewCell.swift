//
//  AppsCollectionViewCell.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit

class AppsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblName : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
//    @IBAction func btn(_ sender: Any) {
//        print("123456alo")
//    }
    func reloadCell(id: String,name: String) {
       // titleLabel.text = name
        if let image = getImageFromId(id: id) {
            imageView.image = image
            imageView.backgroundColor = .clear
            lblName.text = ""
        }else {
            imageView.image = UIImage.init(named: "AppsBackGround")
            lblName.text = name
          //  imageView.backgroundColor = .lightGray
        }
//        if id.isFavourited(){
//            likeButton.setImage(UIImage.init(named: "ic_app_like"), for: .normal)
//        }else{
//            likeButton.setImage(UIImage.init(named: "ic_app_dislike"), for: .normal)
//        }
    }
    
    func getImageFromId(id: String) -> UIImage? {
        switch id {
        case Constants.Apps.amazone.rawValue:
            return .amazone
        case Constants.Apps.appleTV.rawValue:
            return .appleTV
        case Constants.Apps.bluetoothSound.rawValue:
            return .bluetoothSound
        case Constants.Apps.brandShop.rawValue:
            return .brandShop
        case Constants.Apps.broadcastProgram.rawValue:
            return .broadcastProgram
        case Constants.Apps.calendar.rawValue:
            return .calendar
        case Constants.Apps.chanelManager.rawValue:
            return .chanelManager
        case Constants.Apps.cheeringMode.rawValue:
            return .cheeringMode
        case Constants.Apps.customerSupport.rawValue:
            return .customerSupport
        case Constants.Apps.deviceConnector.rawValue:
            return .deviceConnector
        case Constants.Apps.facebook.rawValue:
            return .facebook
        case Constants.Apps.findChanel.rawValue:
            return .findChanel
        case Constants.Apps.fptPlay.rawValue:
            return .fptPlay
        case Constants.Apps.iQIYI.rawValue:
            return .iQIYI
        case Constants.Apps.lgContentStorename.rawValue:
            return .lgContentStorename
        case Constants.Apps.liveplus.rawValue:
            return .liveplus
        case Constants.Apps.lgThinQ.rawValue:
            return .lgThinQ
        case Constants.Apps.media.rawValue:
            return .media
        case Constants.Apps.mp3.rawValue:
            return .mp3
        case Constants.Apps.myTv.rawValue:
            return .myTv
        case Constants.Apps.netflix.rawValue:
            return .netflix
        case Constants.Apps.remoteManagement.rawValue:
            return .remoteManagement
        case Constants.Apps.searchApp.rawValue:
            return .searchApp
        case Constants.Apps.settingApp.rawValue:
            return .settingApp
        case Constants.Apps.tmall.rawValue:
            return .tmall
        case Constants.Apps.viewOn.rawValue:
            return .viewOn
        case Constants.Apps.websiteBrowser.rawValue:
            return .website
        case Constants.Apps.websiteApp.rawValue:
            return .website2
        case Constants.Apps.youtube.rawValue:
            return .youtube
        case Constants.Apps.theFirstUse.rawValue:
            return .theFirstUse
        default:
            return nil
        }
    }
}
extension UIImage {
    static let appDefault = "app".toUIImage()!
    static let appSelected = "app selected".toUIImage()!
    static let remoteDefault = "remote".toUIImage()!
    static let remoteSelected = "remote selected".toUIImage()!
    static let broadCastDefault = "broadcast".toUIImage()!
    static let broadCastSelected = "broadcast selected".toUIImage()!
    static let settingDefault = "setting".toUIImage()!
    static let settingSelected = "setting selected".toUIImage()
    static let favouriteDefault = "favourite".toUIImage()
    static let favouriteSelected = "favourited".toUIImage()
    
    
    //icon
    static let searchIcon = "search".toUIImage()!
    static let closeIcon = "close icon".toUIImage()!
    static let connectedIcon = "connected".toUIImage()!
    static let connectIcon = "turn off".toUIImage()!
    
    
    //apps
    static let advertisement = "advertisement".toUIImage()!
    static let amazone = "amazon".toUIImage()!
    static let appleTV = "apple tv".toUIImage()!
    static let bbcNews = "bbcNews".toUIImage()!
    static let beanBrowser = "bean browser".toUIImage()!
    static let bluetoothSound = "bluetooth sound".toUIImage()!
    static let brandShop = "brand shop".toUIImage()!
    static let broadcastProgram = "broadcast program".toUIImage()!
    static let calendar = "calendar".toUIImage()!
    static let chanelManager = "chanel manager".toUIImage()!
    static let cheeringMode = "cheering mode".toUIImage()!
    static let customerSupport = "customer support".toUIImage()!
    static let dailymotion = "dailymotion".toUIImage()!
    static let deviceConnector = "device connector".toUIImage()!
    static let facebook = "facebook".toUIImage()!
    static let findChanel = "find chanel".toUIImage()!
    static let fptPlay = "fpt play".toUIImage()!
    static let instagram = "instagram".toUIImage()!
    static let iQIYI = "iQIYI".toUIImage()!
    static let lgContentStorename = "lg content storename".toUIImage()!
    static let liveplus = "liveplus".toUIImage()!
    static let lgThinQ = "lq thinq".toUIImage()!
    static let mediaPlugin = "media plugin".toUIImage()!
    static let media = "media".toUIImage()!
    static let mp3 = "mp3".toUIImage()!
    static let myTv = "my tv".toUIImage()!
    static let netflix = "netflix".toUIImage()!
    static let remoteManagement = "remote management".toUIImage()!
    static let searchApp = "search app".toUIImage()!
    static let settingApp = "setting app".toUIImage()!
    static let tmall = "tmall".toUIImage()!
    static let twitter = "twitter".toUIImage()!
    static let ufc = "ufc".toUIImage()!
    static let theFirstUse = "user for the first time".toUIImage()!
    static let viewOn = "viewOn".toUIImage()!
    static let vimeo = "vimeo".toUIImage()!
    static let website = "website".toUIImage()!
    static let website2 = "website 2".toUIImage()!
    static let youtube = "youtube".toUIImage()!
    
    
}

