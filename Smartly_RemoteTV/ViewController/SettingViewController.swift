//
//  SettingViewController.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit
import MessageUI
class SettingViewController: UIViewController, MFMailComposeViewControllerDelegate {
    var array1 = ["Policy","Term","Contact1","About","Share1","Review"]
    var ArrTitle = ["Privacy Policy","Terms and Condistion","Contact us","About us","Share this app","Write a review"]
    var indexSelect = 0
    @IBOutlet weak var SetttingCLV:UICollectionView!
    @IBAction func BtnControl(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        SetttingCLV.register(UINib(nibName: "SettingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SettingCollectionViewCell")
    }
    

  
}
extension SettingViewController: UICollectionViewDelegate, UICollectionViewDataSource {    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SettingCollectionViewCell", for: indexPath) as! SettingCollectionViewCell
        cell.ImageViewa.image = UIImage.init(named: array1[indexPath.row])
        cell.lblNameSetting.text = ArrTitle[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 3{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ContainViewController") as! ContainViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.indexSelect = indexPath.row + 1
            self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.row == 4{
            let text = "Share for every one"
                        let URL:NSURL = NSURL(string: "https://www.google.co.in")! //link
                        let vc = UIActivityViewController(activityItems: [text,URL], applicationActivities: [])
                        if let popoverControler = vc.popoverPresentationController{
                            popoverControler.sourceView = self.view
                            popoverControler.sourceRect = self.view.bounds
                        }
                        self.present(vc, animated: true,completion:  nil)
        }
        else if indexPath.row == 2 {
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([mailSupport])
                mail.setSubject("Regarding Cleaner")
                present(mail, animated: true)
            } else {
                let alert = UIAlertController(title: "Error", message: "Please login your email in the email application", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if indexPath.row == 5{
            if let url = URL.init(string: "https://www.google.co.in"){ // link apps
                if UIApplication.shared.canOpenURL(url){
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
}

extension SettingViewController: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return .zero
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width:collectionView.bounds.width, height: collectionView.bounds.width/6)
        }
        return CGSize(width:collectionView.bounds.width - 20, height: collectionView.bounds.width/6)
    }
}


