//
//  HomeViewController.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit
import ConnectSDK
import SwiftKeychainWrapper
var purchase = 0
let userDefault = UserDefaults.standard
class HomeViewController: UIViewController {
    @IBOutlet weak var BGImageKeyBoard:UIImageView!
    @IBOutlet weak var BGImageTouchPad:UIImageView!
    @IBOutlet weak var BGImageControl:UIImageView!
    @IBOutlet weak var BGImageApps:UIImageView!
    @IBOutlet weak var ContainView:UIView!
    @IBOutlet weak var AppsCLVView:UIView!
    @IBOutlet weak var ChangeView:UIView!
    @IBOutlet weak var TouchPadView:UIView!
    @IBOutlet weak var BtnApps:UIButton!
    @IBOutlet weak var BtnControl:UIButton!
    @IBOutlet weak var BtnKeyBoard:UIButton!
    
    @IBOutlet weak var BtnTouchPad:UIButton!
    @IBOutlet weak var AppsCLV:UICollectionView!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var Run: NSLayoutConstraint!
    //HOME
    var keyboardView: KeyBoarchView!
    var controlview: ControlV!
    var IsMute = false
    var level = 0
    //APPS
    private let remoteManager = ServiceManager.shared
    
    private var allApps = [AppInfo]()
    private var apps = [AppInfo]()
    
    private var lastPosition: (CGFloat, CGFloat)? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        BGImageControl.image = UIImage.init(named: "BtnHome1")
        print("home\(level)")
        if UIDevice.current.userInterfaceIdiom == .pad{
            BtnApps.titleLabel?.font = BtnApps.titleLabel?.font.withSize(26)
            BtnControl.titleLabel?.font = BtnControl.titleLabel?.font.withSize(26)
            BtnKeyBoard.titleLabel?.font = BtnKeyBoard.titleLabel?.font.withSize(26)
            BtnTouchPad.titleLabel?.font = BtnTouchPad.titleLabel?.font.withSize(26)
        }
        //checkRemoteConnected()
        
        setupKeyboardView()
        setupControlView()
        AppsCLVView.isHidden = true
        keyboardView.isHidden = true
        controlview.isHidden = false
        TouchPadView.isHidden = true
        AppsCLV.register(UINib(nibName: "AppsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AppsCollectionViewCell")
        searchTextField.addTarget(self, action: #selector(textFieldTextDidChange), for: .editingChanged)
        NotificationCenter.default
            .addObserver(forName: NSNotification.Name(rawValue: "connectedDevice"), object: nil, queue: .main, using: onDeviceConnected(_:))
    }
    
    func checkRemoteConnected() {
        if ServiceManager.shared.connectableDevice == nil
            || ServiceManager.shared.connectableDevice?.connected == false {
            let alert = UIAlertController(title: "Connect", message: "To use the app, please connected to the TV.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Connect Now", style: .default, handler: { action in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ConnectViewController") as! ConnectViewController
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let SaveLevel:Int =  KeychainWrapper.standard.integer(forKey: "SAVE_LEVEL"){
            level = SaveLevel
        }
        navigationController?.setNavigationBarHidden(true, animated: false)
        reloadData()
        print("HUNG____\(level)")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkRemoteConnected()
        //        nameLabel.text = ServiceManager.shared.connectableDevice?.friendlyName ?? "Smart TV"
    }
    @objc func dismissKeyboard() {
        AppsCLVView.endEditing(true)
    }
    @IBAction func OnConnectWifi(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConnectViewController") as! ConnectViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnSetting(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func BtnApps(_ sender: Any) {
        if level == 2{
            checkRemoteConnected()
            ContainView.isHidden = true
            AppsCLVView.isHidden = false
            reloadData()
            Run.constant = -UIScreen.main.bounds.width/4
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
                self.BGImageApps.image = UIImage.init(named: "BtnHome1")
            }
            //BtnApps.setBackgroundImage(UIImage.init(named: ""), for: .normal)
            BGImageControl.image = UIImage.init(named: "BtnHomeblur1")
            BGImageTouchPad.image = UIImage.init(named: "BtnHomeblur1")
            BGImageKeyBoard.image = UIImage.init(named: "BtnHomeblur1")
            AppsCLV.reloadData()
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PayViewController") as! PayViewController
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen //or .overFullScreen for transparency
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func BtnControl(_ sender: Any) {
        // reloadSegmentView()
        ContainView.isHidden = false
        TouchPadView.isHidden = true
        keyboardView.isHidden = true
        controlview.isHidden = false
        AppsCLVView.isHidden = true
        //BtnControl.setBackgroundImage(UIImage.init(named: ""), for: .normal)
        Run.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.BGImageControl.image = UIImage.init(named: "BtnHome1")
        }
        BGImageApps.image = UIImage.init(named: "BtnHomeblur1")
        
        BGImageTouchPad.image = UIImage.init(named: "BtnHomeblur1")
        BGImageKeyBoard.image = UIImage.init(named: "BtnHomeblur1")
        
    }
    @IBAction func BtnKeyBoard(_ sender: Any) {
        
        ContainView.isHidden = false
        keyboardView.isHidden = false
        TouchPadView.isHidden = true
        AppsCLVView.isHidden = true
        controlview.isHidden = true
        Run.constant = UIScreen.main.bounds.width/2
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.BGImageControl.image = UIImage.init(named: "BtnHome1")
        }
        //  BtnKeyBoard.setBackgroundImage(UIImage.init(named: ""), for: .normal)
        BGImageApps.image = UIImage.init(named: "BtnHomeblur1")
        BGImageControl.image = UIImage.init(named: "BtnHomeblur1")
        BGImageTouchPad.image = UIImage.init(named: "BtnHomeblur1")
        BGImageKeyBoard.image = UIImage.init(named: "BtnHome1")
    }
    @IBAction func BtnTouchPad(_ sender: Any) {
        TouchPadView.isHidden = false
        ContainView.isHidden = false
        keyboardView.isHidden = true
        AppsCLVView.isHidden = true
        controlview.isHidden = true
        // BtnTouchPad.setBackgroundImage(UIImage.init(named: ""), for: .normal)
        Run.constant = UIScreen.main.bounds.width/4
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.BGImageTouchPad.image = UIImage.init(named: "BtnHome1")
        }
        BGImageApps.image = UIImage.init(named: "BtnHomeblur1")
        BGImageControl.image = UIImage.init(named: "BtnHomeblur1")
        //BGImageTouchPad.image = UIImage.init(named: "BtnHome1")
        BGImageKeyBoard.image = UIImage.init(named: "BtnHomeblur1")
    }
    @IBAction func OnTurnOff(_ sender: Any) {
        let alert = UIAlertController(title: "Confirm", message: "Do you want to turn off the TV?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] action in
            self?.handle(key: .power)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: -- Action handle bottom control
    
    @IBAction func onMute(_ sender: Any) {
        //        if (IsMute){
        //            muteButton.imageView?.image = UIImage.init(named: "Voice1")
        //        }else{
        //            muteButton.imageView?.image = UIImage.init(named: "muteq")
        //        }
        handle(key: .mute)
        IsMute = !IsMute
        
    }
    
    @IBAction func onHome(_ sender: Any) {
        handle(key: .home)
    }
    
    @IBAction func onPrevious(_ sender: Any) {
        handle(key: .previous)
    }
    
    @IBAction func onNext(_ sender: Any) {
        handle(key: .next)
    }
    
    @IBAction func onStop(_ sender: Any) {
        handle(key: .pause)
    }
    
    @IBAction func onPlay(_ sender: Any) {
        handle(key: .play)
    }
    
    @IBAction func onVolUp(_ sender: Any) {
        handle(key: .volumeUp)
    }
    
    @IBAction func onVolDown(_ sender: Any) {
        handle(key: .volumeDown)
    }
    
    @IBAction func onChUp(_ sender: Any) {
        handle(key: .chUp)
    }
    
    @IBAction func onChDown(_ sender: Any) {
        handle(key: .chDown)
    }
    func reloadView(){
        if AppManager.shared().isMute{
            muteButton.setBackgroundImage(UIImage.init(named: "muteq"), for: .normal)
        }else{
            muteButton.setBackgroundImage(UIImage.init(named: "Voice1"), for: .normal)
        }
    }
    
    @objc func onDeviceConnected(_ notification: Notification) {
        ServiceManager.shared.subscribeMute(onSuccess: { [weak self] in
            AppManager.shared().isMute = $0
            self?.reloadView()
        })
    }
    
    
    private func handle(key: Constants.RemoteControl) {
        if ServiceManager.shared.connectableDevice == nil
            || ServiceManager.shared.connectableDevice?.connected == false {
            checkRemoteConnected()
            return
        }
        if case .voice = key {
            self.showError(message: "This feature is not supported")
        } else {
            ServiceManager.shared.handle(key)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        controlview.frame = ChangeView.bounds
        controlview.layoutIfNeeded()
        
        keyboardView.frame = ChangeView.bounds
        keyboardView.layoutIfNeeded()
    }
    
    func setupKeyboardView(){
        keyboardView = UIView.loadFromNibNamed(nibNamed: "KeyBoarchView") as? KeyBoarchView
        keyboardView.backgroundColor = .clear
        ChangeView.addSubview(keyboardView)
        for item in keyboardView.buttons{
            item.addTarget(self, action: #selector(onNumber(_:)), for: .touchUpInside)
        }
    }
    func setupControlView(){
        controlview = UIView.loadFromNibNamed(nibNamed: "ControlV") as? ControlV
        controlview.backgroundColor = .clear
        ChangeView.addSubview(controlview)
        controlview.topButton.addTarget(self, action: #selector(onTopControlUp(_:)), for: .touchUpInside)
        controlview.bottomButton.addTarget(self, action: #selector(onTopControlDown(_:)), for: .touchUpInside)
        controlview.leftButton.addTarget(self, action: #selector(onTopControlLeft(_:)), for: .touchUpInside)
        controlview.rightButton.addTarget(self, action: #selector(onTopControlRight(_:)), for: .touchUpInside)
        controlview.centerButton.addTarget(self, action: #selector(onTopControlOK(_:)), for: .touchUpInside)
        controlview.returnButton.addTarget(self, action: #selector(onTopControlReturn(_:)), for: .touchUpInside)
        controlview.inputButton.addTarget(self, action: #selector(onTopControlInput(_:)), for: .touchUpInside)
        controlview.exitButton.addTarget(self, action: #selector(onTopControlExit(_:)), for: .touchUpInside)
    }
    func reloadData(){
        remoteManager.getAppList(onSuccess: { [weak self] apps in
            self?.allApps.removeAll()
            let temp = apps.filter{ Constants.Apps.allCases.map{ $0.rawValue }.contains($0.id)}.sorted(by: { app1, app2 in
                return app1.name <= app2.name
            })
            self?.allApps.append(contentsOf: temp)
            self?.reloadFilterData()
        }, onFailure: { [weak self] error in
            self?.showError(message: "\(error?.localizedDescription ?? "Load data error")")
        })
    }
    func reloadFilterData(){
        apps.removeAll()
        if let text = searchTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), text.count > 0{
            let searchStr = searchTextField.text ?? ""
            apps = allApps.filter({ appinfo in
                return appinfo.name.lowercased().contains(searchStr.lowercased())
            })
        }else{
            apps.append(contentsOf:allApps.sorted(by: { app1, app2 in
                if app1.id.isFavourited(){
                    return true
                }
                return false
            }))
        }
        AppsCLV.reloadData()
    }
    
    @objc func onTopControlUp(_ sender: UIButton) {
        handle(key: .up)
    }
    
    @objc func onTopControlDown(_ sender: Any) {
        handle(key: .down)
    }
    
    @objc func onTopControlLeft(_ sender: Any) {
        handle(key: .left)
    }
    
    @objc func onTopControlRight(_ sender: Any) {
        handle(key: .right)
    }
    
    @objc func onTopControlOK(_ sender: Any) {
        handle(key: .ok)
    }
    @objc func onNumber(_ sender: UIButton) {
        let tag = sender.tag - 1000
        print(tag)
        if tag == 0{
            handle(key: .number(0))
        }else if tag == 1{
            handle(key: .number(1))
        }else if tag == 2{
            handle(key: .number(2))
        }else if tag == 3{
            handle(key: .number(3))
        }else if tag == 4{
            handle(key: .number(4))
        }else if tag == 5{
            handle(key: .number(5))
        }else if tag == 6{
            handle(key: .number(6))
        }else if tag == 7{
            handle(key: .number(7))
        }else if tag == 8{
            handle(key: .number(8))
        }else if tag == 9{
            handle(key: .number(9))
        }else if tag == 10{
            //                        handle(key: .numberDelete)
            //Delete
        }else if tag == 11{
            handle(key: .numberNone)
            //None
        }
    }
    
    //MARK: -- Action handle source control
    @objc func onTopControlExit(_ sender: Any) {
        handle(key: .exit)
    }
    //    @IBAction func onTopControlExit(_ sender: Any) {
    //        handle(key: .exit)
    //    }
    //
    //    @IBAction func onSource(_ sender: Any) {
    //        handle(key: .source(Constants.sourceId))
    //    }
    //
    //    @IBAction func onGuide(_ sender: Any) {
    //        handle(key: .source(Constants.guideId))
    //    }
    //
    @objc func onTopControlInput(_ sender: Any) {
        handle(key: .source(Constants.sourceId))
    }
    @objc func onTopControlReturn(_ sender: Any) {
        handle(key: .back)
    }
    //    @IBAction func onTopControlReturn(_ sender: Any) {
    //        handle(key: .back)
    //    }
    @IBAction func touchpadHandleTapGesture(_ sender: UITapGestureRecognizer) {
        handle(key: .click)
        //   print("hung123")
    }
    
    @IBAction func touchpadHandlePanGesture(_ sender: UIPanGestureRecognizer) {
        //   print("hung123")
        switch sender.state{
        case .began:
            let transaction = sender.translation(in: TouchPadView)
            lastPosition = (transaction.x, transaction.y)
        case .cancelled:
            break
        case .changed:
            let transaction = sender.translation(in: TouchPadView)
            var dx: CGFloat = 0.0
            var dy: CGFloat = 0.0
            
            if lastPosition != nil {
                dx = transaction.x - (lastPosition?.0 ?? 0)
                dy = transaction.y - (lastPosition?.1 ?? 0)
            }
            lastPosition = (transaction.x, transaction.y)
            if sender.numberOfTouches >= 2 {
                handle(key: .scroll(dx, dy))
            } else {
                handle(key: .move(dx, dy))
            }
        case .ended:
            break
        default: ()
        }
    }
}
extension HomeViewController: UITextFieldDelegate {
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @objc func textFieldTextDidChange(){
        reloadFilterData()
    }
}
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    //    func numberOfSections(in collectionView: UICollectionView) -> Int {
    //        return 1
    //    }
    //
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return apps.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppsCollectionViewCell.className, for: indexPath) as! AppsCollectionViewCell
        let app = apps[indexPath.row]
        cell.reloadCell(id: app.id,name: app.name)
        //        cell.likeButton.tag = indexPath.row + 1000
        //        cell.likeButton.addTarget(self, action: #selector(onFavorites(_:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("hung123")
        let app = apps[indexPath.row]
        remoteManager.launchApp(id: app.id, onSuccess: { [weak self] _ in
            self?.showSuccess(message: "Running app \(app.name ?? "") success")
        }, onFailure: { [weak self] _ in
            self?.showError(message: "Running app \(app.name ?? "") failed")
        })

    }
}

//MARK: -- UICollectionViewDelegateFlowLayout


extension HomeViewController: UICollectionViewDelegateFlowLayout {
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return .zero
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width:collectionView.bounds.width/3 - 20, height: collectionView.bounds.width/3)
        }
        return CGSize(width:collectionView.bounds.width/2 - 10, height: collectionView.bounds.width/2 - 10)
    }
}

