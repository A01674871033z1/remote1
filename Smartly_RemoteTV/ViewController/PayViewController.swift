//
//  PayViewController.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit
import SwiftyStoreKit
//import Toast_Swift
import AppsFlyerLib
import SwiftKeychainWrapper
class PayViewController: UIViewController {
    var themeId = "from_home_page"
    var level = 0
    var purchaseType: PurchaseType = .monthly
    enum PurchaseType: Int {
        case weekly
        case monthly
        case yearly
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func gotoPremiumWEEK(_ sender: Any) {
        purchaseType = .weekly
        purchase(type: .weekly)
    }
    @IBAction func gotoPremiumMONTH(_ sender: Any) {
        purchaseType = .monthly
        purchase(type: .monthly)
    }
    @IBAction func gotoPremiumYEAR(_ sender: Any) {
        purchaseType = .yearly
        purchase(type: .yearly)
    }
    @IBAction func Continue(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.level = level
        vc.modalPresentationStyle = .overFullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
    }
    func purchase(type: PurchaseType){
        var productId = PRODUCT_ID_1MONTH
        var revenue = revenueWEEK
        if  type == .weekly {
            productId = PRODUCT_ID_1MONTH
            revenue = revenueWEEK
        }else if  type == .monthly {
            productId = PRODUCT_ID_6MONTH
            revenue = revenueMONTH
        }else if  type == .yearly {
            productId = PRODUCT_ID_1YEAR
            revenue = revenueYEAR
        }
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
            switch result {
            case .success(_):
                print("ok 1")
                self.verifyPurchase()
                self.level = 2
                KeychainWrapper.standard.set(self.level, forKey: "SAVE_LEVEL")
//                FirebaseService.shared.updateLevelUser(id: id, level: level)
//                UserAccountModel.shared.saveUserLevel()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                vc.modalTransitionStyle = .crossDissolve
                vc.level = self.level
                vc.modalPresentationStyle = .overFullScreen //or .overFullScreen for transparency
                self.present(vc, animated: true, completion: nil)
                AppsFlyerLib.shared().logEvent("af_purchase",
                withValues: [
                    AFEventParamContentId:productId,
                    AFEventParamContentType : productId,
                    AFEventParamRevenue: revenue,
                    AFEventParamCurrency:"USD",
                    "af_source_themeid": self.themeId
                ]);
            case .error(let error):
                print(error.localizedDescription)
                self.showError(message: error.localizedDescription)
            }
        }
    }
    func verifyPurchase(){
        print("verifyPurchase")
        //self.showLoading()
        var appleValidator: AppleReceiptValidator!
        if iS_TEST{
            appleValidator = AppleReceiptValidator(service: .sandbox, sharedSecret: PRODUCT_SHARED_SECRET)
        }else{
            appleValidator = AppleReceiptValidator(service: .production, sharedSecret: PRODUCT_SHARED_SECRET)
        }
        SwiftyStoreKit.verifyReceipt(using: appleValidator) {[weak self] result in
            self?.hideLoading()
            switch result {
            case .success(let receipt):
                let productIds = Set.init(PRODUCT_IDS)
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                switch purchaseResult {
                case .purchased(let expiryDate, _):
                    self?.showSuccess(message: "Your purchase is success")
                   // self?.reloadView()
                case .expired(let expiryDate, _): break
                case .notPurchased:
                    self?.showError(message: "The user has never purchased")
                }
            case .error(let error):
                print(error.localizedDescription)
                self?.verifyPurchaseSandbox()
            }
        }
    }
   
   func verifyPurchaseSandbox(){
       print("verifyPurchaseSandbox")
       //self.showLoading()
       let appleValidator: AppleReceiptValidator = AppleReceiptValidator(service: .sandbox, sharedSecret: PRODUCT_SHARED_SECRET)
       SwiftyStoreKit.verifyReceipt(using: appleValidator) {[weak self] result in
           self?.hideLoading()
           switch result {
           case .success(let receipt):
               let productIds = Set.init(PRODUCT_IDS)
               let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
               switch purchaseResult {
               case .purchased(let expiryDate, _):
                   self?.showSuccess(message: "Your purchase is success")
                  
                   //self?.reloadView()
               case .expired(let expiryDate, _): break
//                   self?.reloadView()
               case .notPurchased:
                   self?.showError(message: "The user has never purchased")
               }
           case .error(let error):
               print("verifyPurchase error")
               self?.showError(message: error.localizedDescription)
               print(error.localizedDescription)
           }
       }
   }
   

}
