//
//  ConnectViewController.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 14/2/2022.
//

import UIKit
import ConnectSDK

class ConnectViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var refressButton: UIButton!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    private let deviceManager = DeviceManager.shared
    private let serviceManager = ServiceManager.shared
    
    private var devices: [ConnectableDevice] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.userInterfaceIdiom == .pad{
            title1.font = emptyLabel.font.withSize(26)
            title2.font = emptyLabel.font.withSize(20)
            nameLabel.font = nameLabel.font.withSize(36)
            emptyLabel.font = emptyLabel.font.withSize(18)
        }
        collectionView.register(UINib(nibName: ConnectCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ConnectCollectionViewCell.className)
        deviceManager.delegate = self
        startScanDevice()
        setupEvents()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deviceManager.stopScanDevice()
    }
    
    private func connectToDevice(device: ConnectableDevice){
        if serviceManager.connectableDevice != nil,
           serviceManager.connectableDevice?.connected == true{
            serviceManager.disconnect()
        }
        serviceManager.connectableDevice = device
        serviceManager.connect()
    }
    
    private func startScanDevice(){
        devices.removeAll()
        collectionView.reloadData()
        loadingView.startAnimating()
        loadingView.isHidden = false
        deviceManager.startScanDevice()
        refressButton.isEnabled = false
        emptyLabel.isHidden = true
        refressButton.setTitleColor(UIColor.lightGray, for: .normal)
        refressButton.cornerRadiusV = 8.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
            self?.stopScanDevice()
        }
    }
    
    private func stopScanDevice(){
        if devices.count == 0{
            emptyLabel.isHidden = false
        }else{
            emptyLabel.isHidden = true
        }
        loadingView.stopAnimating()
        loadingView.isHidden = true
        deviceManager.stopScanDevice()
        refressButton.isEnabled = true
        refressButton.setTitleColor(UIColor.white, for: .normal)
        refressButton.cornerRadiusV = 8.0
    }
        
    private func setupEvents() {
        serviceManager.delegate(self) { (`self`, state) in
            switch state {
            case .connectableDevicePairingSuccess, .onDeviceReady:
                `self`.onClose()
            case .onConnectionFailed(let message):
                `self`.showError(message: message)
            case .onCapabilityUpdated:
                break
            case .onDeviceDisconnect:
                break
            case .onParingRequire( _):
                break
            case .connectableDeviceConnectionSuccess:
                break
            case .connectableDeviceConnectionRequired:
                break
                //Please accept connect in your TV
            }
        }
    }
            
    private func onClose() {
        deviceManager.stopScanDevice()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: action
    
    @IBAction private func onRefress(_ sender: Any) {
        startScanDevice()
    }
    
    @IBAction func onClose(_ sender: Any) {
        onClose()
    }
}

//MARK: -- DeviceManagerDelegate

extension ConnectViewController : DeviceManagerDelegate {
    func didFindDevice(_ device: ConnectableDevice) {
        devices.removeAll()
        devices.append(contentsOf: deviceManager.devices)
        collectionView.reloadData()
        loadingView.stopAnimating()
        loadingView.isHidden = true
    }
    
    func didLostDevice(_ device: ConnectableDevice) {
        devices.removeAll()
        devices.append(contentsOf: deviceManager.devices)
        collectionView.reloadData()
    }
    
    func didUpdateDevice(_ device: ConnectableDevice) {
        
        print(device.friendlyName ?? "No Device")
        print(device.modelName ?? "No Device")
        print(device.modelNumber ?? "No Device")
        
        loadingView.stopAnimating()
        loadingView.isHidden = true
        devices.removeAll()
        devices.append(contentsOf: deviceManager.devices)
        collectionView.reloadData()
    }
    
    func didFailWithError(_ error: Error) {
        showError(message: error.localizedDescription)
        loadingView.stopAnimating()
        loadingView.isHidden = true
    }
}

//MARK: -- UICollectionViewDelegate, UICollectionViewDataSource

extension ConnectViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return devices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConnectCollectionViewCell.className, for: indexPath) as! ConnectCollectionViewCell
        let item = devices.unique()[indexPath.row]
        if let device = serviceManager.connectableDevice
           , device.connected == true{
            if device.id == item.id{
                cell.colorView.backgroundColor = .white
                cell.nameLabel.textColor = .black
            }else{
                cell.colorView.backgroundColor = .white
                cell.nameLabel.textColor = .black
            }
        }else{
            cell.colorView.backgroundColor = .white
            cell.nameLabel.textColor = .black
        }
        cell.updateCell(name: item.friendlyName)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        connectToDevice(device: devices[indexPath.row])
    }
}
//MARK: -- UICollectionViewDelegateFlowLayout

extension ConnectViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return CGSize(width: collectionView.bounds.width/2, height: 70)
        }
        let height: CGFloat =  58
        let width: CGFloat = DEVICE_WIDTH - 60
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 20, left: 30, bottom: 20, right: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
