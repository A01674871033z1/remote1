//
//  AppDelegate.swift
//  Smartly_RemoteTV
//
//  Created by Apple on 13/2/2022.
//

import UIKit
import SwiftyStoreKit


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // var appOpenAd: GADAppOpenAd?
    var loadTime: Date = Date()
    var loadError: Int = 0
    var isRequestTrackingAuthorization = false
    var isTheFirst = true
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
               window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
               window?.makeKeyAndVisible()
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
                for purchase in purchases {
                    switch purchase.transaction.transactionState {
                    case .purchased, .restored:
                        if purchase.needsFinishTransaction {
                            // Deliver content from server, then:
                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                        }
                        // Unlock content
                    case .failed, .purchasing, .deferred:
                        break // do nothing
                    @unknown default:
                       print("Fail")
                    }
                }
            }
               return true
//        self.window = UIWindow.init(frame: UIScreen.main.bounds)
//        window!.makeKeyAndVisible()
     //   let homeVC = HomeViewController.init(nibName: HomeViewController.className.nibWithNameiPad(), bundle: nil)
      //  window!.rootViewController = BaseNavigationViewController.init(rootViewController: homeVC)
        
        //  GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        //  AdmobManager.shared.fullRootViewController = window?.rootViewController!
        
     //   return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
        ServiceManager.shared.disconnect()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        ServiceManager.shared.connect()
        //        if isTheFirst{
        //            isTheFirst = false
        //            if #available(iOS 14, *) {
        //     //           ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
        //                    self.isRequestTrackingAuthorization = true
        //                    AdmobManager.shared.createAndLoadInterstitial()
        //                })
        //            } else {
        //                isRequestTrackingAuthorization = true
        //                AdmobManager.shared.createAndLoadInterstitial()
        //            }
        //        }
        //        if isRequestTrackingAuthorization {
        //            tryToPresentAd()
        //        }
    }
    
    //    func requestOpenAds(){
    //        print("requestOpenAds" )
    //
    //        if loadError >= 2{
    //            return
    //        }
    //
    //        appOpenAd = nil
    //        let request = GADRequest()
    //        GADAppOpenAd.load(withAdUnitID: openAds, request: request, orientation: .portrait) { (openAds, error) in
    //            if let er = error{
    //                print("Open ads error" + er.localizedDescription)
    //                self.loadError = self.loadError + 1
    //            }else{
    //                print("Open ads Ok" )
    //                self.appOpenAd = openAds
    //                self.appOpenAd?.fullScreenContentDelegate = self
    //                self.loadTime = Date()
    //            }
    //        }
    //    }
    
    //    func tryToPresentAd(){
    //        if self.appOpenAd != nil && wasLoadTimeLessThan4HoursAgo(){
    //            self.appOpenAd?.present(fromRootViewController: window!.rootViewController!)
    //        }else{
    //            requestOpenAds()
    //        }
    //    }
    
    func wasLoadTimeLessThan4HoursAgo() -> Bool{
        let now = Date()
        let timeIntervalBetweenNowAndLoadTime = now.timeIntervalSince(self.loadTime)
        let secondsPerHour = 3600.0
        let intervalInHours = timeIntervalBetweenNowAndLoadTime / secondsPerHour
        if intervalInHours < 4.0{
            return true
        }
        return false
    }
}

//extension AppDelegate: GADFullScreenContentDelegate{
//    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
//        print("didFailToPresentFullScreenContentWithError" + error.localizedDescription)
//        self.requestOpenAds()
//    }
//
//    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("adDidPresentFullScreenContent")
//    }
//
//    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("adDidDismissFullScreenContent")
//        self.requestOpenAds()
//    }
//}
